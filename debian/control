Source: kio-extras5
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               extra-cmake-modules,
               gperf,
               libappimage-dev,
               libimobiledevice-dev,
               libkdsoap5-dev,
               libkf5activities-dev (>= 5.3.0~),
               libkf5activitiesstats-dev,
               libkf5archive-dev (>= 5.40.0~),
               libkf5bookmarks-dev (>= 5.40.0~),
               libkf5config-dev (>= 5.40.0~),
               libkf5configwidgets-dev (>= 5.40.0~),
               libkf5coreaddons-dev (>= 5.40.0~),
               libkf5dbusaddons-dev (>= 5.40.0~),
               libkf5dnssd-dev (>= 5.40.0~),
               libkf5doctools-dev (>= 5.40.0~),
               libkf5guiaddons-dev (>= 5.40.0~),
               libkf5i18n-dev (>= 5.40.0~),
               libkf5iconthemes-dev (>= 5.40.0~),
               libkf5kcmutils-dev,
               libkf5kexiv2-dev,
               libkf5khtml-dev (>= 5.3.0),
               libkf5kio-dev (>= 5.40.0~),
               libkf5pty-dev (>= 5.40.0~),
               libkf5solid-dev (>= 5.40.0~),
               libkf5syntaxhighlighting-dev (>= 5.40.0~),
               libmtp-dev,
               libopenexr-dev,
               libphonon4qt5-dev (>= 4:4.6.60),
               libphonon4qt5experimental-dev,
               libqt5svg5-dev (>= 5.5.0~),
               libsmbclient-dev,
               libssh-dev (>= 0.6.0),
               libtag1-dev (>= 1.11~),
               libtirpc-dev,
               pkg-config,
               pkg-kde-tools,
               qcoro-qt5-dev,
               qtbase5-dev (>= 5.5.0~),
               shared-mime-info
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/kde/workspace/kio-extras
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kio-extras
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kio-extras.git

Package: kio-extras5
Architecture: any
Depends: kio-extras5-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: kio-extras (<< 4:24.01)
Replaces: kio-extras (<< 4:24.01)
Description: Extra functionality for kioslaves.
 A kioslave is a plugin designed to be intimately familiar with a certain
 protocol, so that a standardized interface can be used to get at data from
 any number of places.  A few examples are the http and ftp kioslaves,
 which using nearly identical methods will retrieve data from an http or
 ftp server respectively.
 .
 This package is part of the KDE base workspace module.

Package: kio-extras5-data
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Breaks: kio-extras-data (<< 4:24.01)
Replaces: kio-extras-data (<< 4:24.01)
Description: Extra functionality for kioslaves data files.
 A kioslave is a plugin designed to be intimately familiar with a certain
 protocol, so that a standardized interface can be used to get at data from
 any number of places.  A few examples are the http and ftp kioslaves,
 which using nearly identical methods will retrieve data from an http or
 ftp server respectively.
 .
 This package contains the data files
